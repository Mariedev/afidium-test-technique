/**
 * Initial State
 */
const initialState = {
  flightsList: [],
  loading: true,
};

/**
 * Types
 */
export const LOAD_FLIGHTS = 'LOAD_FLIGHTS';
export const RECEIVED_FLIGHTS = 'RECEIVED_FLIGHTS';
/**
 * Traitements
 */

/**
 * Reducer
 */
const reducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_FLIGHTS:
      return {
        ...state,
      };
    case RECEIVED_FLIGHTS:
      return {
        ...state,
        flightsList: [...action.allFlights],
        loading: false,
      };
    default:
      return state;
  }
};

/**
 * Action Creators
 */
export const loadFlights = () => ({
  type: LOAD_FLIGHTS,
});
export const receivedFlights = allFlights => ({
  type: RECEIVED_FLIGHTS,
  allFlights,
});
/**
 * Selectors
 */

/**
 * Export
 */
export default reducer;

import axios from 'axios';
import { LOAD_FLIGHTS, receivedFlights } from 'src/store/reducer';

const url = 'https://www.ghusse.com/afidium/response.json';

const ajaxMiddleware = store => next => (action) => {
  switch (action.type) {
    case LOAD_FLIGHTS:
      axios.get(url)
        .then((result) => {
          store.dispatch(receivedFlights(result.data));
        })
        .catch((error) => {
          console.log(error);
        });
      break;
    default:
      next(action);
  }
};
export default ajaxMiddleware;

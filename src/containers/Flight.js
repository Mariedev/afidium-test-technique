/* eslint-disable linebreak-style */
import { connect } from 'react-redux';
import { getAllFlightsInfos } from 'src/data/flightInfos';
import Flight from 'src/components/Flight';

const mapStateToProps = state => ({
  flights: getAllFlightsInfos(state.allFlight.flightsList),
});

const mapDispatchToProps = {};

const FlightContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Flight);

export default FlightContainer;

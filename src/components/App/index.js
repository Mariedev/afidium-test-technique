/**
 * Npm import
 */
import React from 'react';

/**
 * Local import
 */
import Flight from 'src/components/Flight';

import { getAllFlightsInfos } from 'src/data/flightInfos';
import 'src/data/data.json';

import './app.scss';

/**
 * Code
 */
console.log(getAllFlightsInfos());


const App = () => (
  <div id="app">
    <Flight flights={getAllFlightsInfos()} />
  </div>
);

/**
 * Export
 */
export default App;

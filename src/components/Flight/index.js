/* eslint-disable linebreak-style */
import React from 'react';
import Proptypes from 'prop-types';
import { Grid, Segment, List } from 'semantic-ui-react';
import './style.scss';
import 'src/data/data.json';


const Flight = ( { flights } ) => (
  <div id="app-content">
    <h1>Application de recherche de dossiers de voyage</h1>
    <List>
      {flights.map(flight => ( 
        <Segment key={flight.code}>    
          <List.Item>
            <Grid columns={3} relaxed="very">

              <Grid.Column>   
                <p>Statut : {flight.status}</p>
                <List.Header >{flight.code}</List.Header>
              </Grid.Column> 

              <Grid.Column >
                <ul>
                  <li>{flight.customerFirstname} {flight.customerLastname} {flight.traveller.length} voyageur(s) 
                    {/* <Voy voy={flight.traveller.length} /> */} 
                  </li> 
                  <li>trajet</li>
                  <li>du {new Date(flight.begin).toLocaleDateString('fr')} au {new Date(flight.end).toLocaleDateString('fr')}</li>
                  {/* <Dates date={flight.begin} displaybegin={new Date(flight.begin).toLocaleDateString('fr')} displayEnd={new Date(flight.end).toLocaleDateString('fr')} /> */}
                </ul> 
              </Grid.Column>
              
           {/*   { flight.total.map(amount => (  */}
              <Grid.Column>
                <p> {/* <Amount {...amount} /> */} Amount €</p>
              </Grid.Column>
       {/*  ))} */}  

            </Grid>
          </List.Item>
        </Segment>

      ))}
    </List>
  </div>
);

Flight.propTypes = {
  flights: Proptypes.arrayOf(
    Proptypes.shape({
      status: Proptypes.string.isRequired,
      code: Proptypes.string.isRequired,
      host: Proptypes.string.isRequired,
      customerFirstname: Proptypes.string,
      customerLastname: Proptypes.string,
      traveller: Proptypes.string.isRequired,
      total: Proptypes.arrayOf(
        Proptypes.shape({
          amount: Proptypes.string.isRequired,
        }).isRequired,
      ).isRequired,
      begin: Proptypes.string.isRequired,
      end: Proptypes.string.isRequired,
    }).isRequired,
  ).isRequired,
};

/* const Status = ({ flights }) => { 
  if (flights.map(flight => ( flight.status )) === 'actionRequired') {
    Document.write(<Icon name="exclamation circle" />)
  } else if (flights.map(flight => ( flight.status )) === 'cancel') {
    Document.write(<Icon name="remove circle" />)
  } else if (flights.map(flight => ( flight.status )) === 'confirm') {
    Document.write(<Icon name="check circle" />)
  } else if (flights.map(flight => ( flight.status )) === 'information') {
    Document.write(<Icon name="info circle" />)
  }};



  



/* function Trav() {
  if (voy.voy === 1) {
    console.log('voy = ', voy.voy)
    const text = document.getElementsByClassName('voy')
    console.log(text);
    text.innertext = 'voyageur';
  } else if (voy.voy > 1) {
    console.log('voy = ', voy.voy)
    const text = document.getElementsByClassName('voy')
    text.innertext = 'voyageurs';
  }
}
*/
 

/* const Amount = ({ amount }) => (
  <Grid.Column>
    <p> {console.log(amount)} €</p>
  </Grid.Column>
); */





/* const Dates = ({ dateBegin, dateEnd, displayEnd, displaybegin }) => {
  if (dateBegin === undefined)  {
    console.log('je rentre dans le if')
  } else if (dateEnd === undefined) {
    console.log('je rentre dans le else if');
  } else {  
    console.log('je rentre dans le else');
    const Show = () => (
      <li>du {displaybegin} au {displayEnd}</li>
    );
    return Show();
  }
};
 */

 
export default Flight;
